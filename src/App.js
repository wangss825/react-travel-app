import React from 'react';

import './App.css';
import Home from './pages/Home';
import Travels from './pages/Travels';
import SingleTravel from './pages/SingleTravel';
import Error from './pages/Error';
import About from './pages/About';
import NavBar from "./components/NavBar";
import Login from "./pages/Login";
import Register from "./pages/Register";


import {Route, Switch} from 'react-router-dom';


function App() {
  return (

    <>
    <NavBar />
    <Switch>
    <Route exact path='/' component={Home} />
    <Route exact path='/travels' component={Travels} />
    <Route exact path='/travels/:slug' component={SingleTravel} />
    <Route exact path='/about' component={About} />
    <Route exact path='/login' component={Login} />
    <Route exact path='/register' component={Register} />
    <Route component={Error} />
    </Switch>

</>
  );
}

export default App;
