

import pic1 from "./images/pic1.jpg";
import pic2 from "./images/pic2.jpg";
import pic3 from "./images/pic3.jpg";
import pic4 from "./images/pic4.jpg";
import pic5 from "./images/pic5.jpg";
import pic6 from "./images/pic6.jpg";
import pic7 from "./images/pic7.jpg";
import pic8 from "./images/pic8.jpg";
import pic9 from "./images/pic9.jpg";
import pic10 from "./images/pic10.jpg";
import pic11 from "./images/pic11.jpg";
import pic12 from "./images/pic12.jpg";

import pic13 from "./images/pic13.jpg";
import pic14 from "./images/pic14.jpg";
import pic15 from "./images/pic15.jpg";
import pic16 from "./images/pic16.jpg";

import pic17 from "./images/pic17.jpg";
import pic18 from "./images/pic18.jpg";
import pic19 from "./images/pic19.jpg";
import pic20 from "./images/pic20.jpg";


import pic21 from "./images/pic21.jpg";
import pic22 from "./images/pic22.jpg";
import pic23 from "./images/pic23.jpg";
import pic24 from "./images/pic24.jpg";

import pic25 from "./images/pic25.jpg";
import pic26 from "./images/pic26.jpg";
import pic27 from "./images/pic27.jpg";
import pic28 from "./images/pic28.jpg";

import pic29 from "./images/pic29.jpg";
import pic30 from "./images/pic30.jpg";
import pic31 from "./images/pic31.jpg";
import pic32 from "./images/pic32.jpg";

import pic33 from "./images/pic33.jpg";
import pic34 from "./images/pic34.jpg";
import pic35 from "./images/pic35.jpg";
import pic36 from "./images/pic36.jpg";


import pic37 from "./images/pic37.jpg";
import pic38 from "./images/pic38.jpg";
import pic39 from "./images/pic39.jpg";
import pic40 from "./images/pic40.jpg";

import pic41 from "./images/pic41.jpg";
import pic42 from "./images/pic42.jpg";
import pic43 from "./images/pic43.jpg";
import pic44 from "./images/pic44.jpg";

import pic45 from "./images/pic45.jpg";
import pic46 from "./images/pic46.jpg";
import pic47 from "./images/pic47.jpg";
import pic48 from "./images/pic48.jpg";


export default [
 //USA
  {
    sys: {
      id: "1"
    },
    fields: {
      name: "California",
      slug: "California",
      type: "USA",
      price: 1400,
      length: 20,


      flight: true,
      featured: false,
      description:
      "California is full of superlatives: It is home to the highest (Mount Whitney) and lowest (Badwater, Death Valley) points in the contiguous United States. In California you'll find the largest living things on earth Sequoiadendron giganteum (the giant Sequoia), as well as the oldest living things on earth Pinus longaeva (Bristlecone Pine), and if that's not enough, you'll also find the tallest living things on earth Sequoia sempervirens (Coast Redwood).  It is the most populous state in the United States--exceeding entire countries like Canada and Australia. It dwarfs countless European countries in geographic scale. If it were its own country, it would be one of the ten largest economies in the world.",
      extras: [
        "Travel Management ",
        "Passenger assistance",
        "Consulting",
        "Marketing and internal communication",
        "Adequate safety/security",
        "Internet",
        "Leisure Services"
      ],
      images: [
        {
          fields: {
            file: {
              url: pic1
            }
          }
        },
        {
          fields: {
            file: {
              url: pic2
            }
          }
        },
        {
          fields: {
            file: {
              url: pic3
            }
          }
        },
        {
          fields: {
            file: {
              url: pic4
            }
          }
        }

      ]
    }
  },
  {
    sys: {
      id: "2"
    },
    fields: {
      name: "Las Vegas",
      slug: "Las Vegas",
      type: "USA",
      price: 1500,
      length: 10,


      flight: false,
      featured: false,
      description:
      "Las Vegas was officially founded in 1905 when the San Pedro, Los Angeles & Salt Lake Railroad was completed. The rail spur, built at the end of Fremont Street, anchored the town and gave it daily life as visitors, food, and mail arrived each afternoon. Visitors didn't find much.",
       extras: [
         
        "Passenger assistance",
        "Consulting",
        "Marketing and internal communication",
        "Adequate safety/security",
        "Internet",
        "Leisure Services"
      ],
      images: [
        {
          fields: {
            file: {
              url: pic5
            }
          }
        },
        {
          fields: {
            file: {
              url: pic6
            }
          }
        },
        {
          fields: {
            file: {
              url: pic7
            }
          }
        },
        {
          fields: {
            file: {
              url: pic8
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "3"
    },
    fields: {
      name: "Hawaii",
      slug: "Hawaii",
      type: "USA",
      price: 1600,
      length: 8,


      flight: false,
      featured: false,
      description:
      "n the 1800s Hawaii was discovered for its economic value (land, whales, people) and settled by peoples from all over the world. The United States annexed Hawaii in 1898 and it became a United States Territory in 1900. ... In 1959 Hawaii became the 50th state in the United States of America.",
       extras: [
        "Travel Management ",
        "Passenger assistance",
        
        "Marketing and internal communication",
        "Adequate safety/security",
         
        "Leisure Services"
      ],
      images: [
        {
          fields: {
            file: {
              url: pic9
            }
          }
        },
        {
          fields: {
            file: {
              url: pic10
            }
          }
        },
        {
          fields: {
            file: {
              url: pic11
            }
          }
        },
        {
          fields: {
            file: {
              url: pic12
            }
          }
        }
      ]
    }
  },
  //European

  {
    sys: {
      id: "4"
    },
    fields: {
      name: "Italy",
      slug: "Italy",
      type: "European",
      price: 1400,
      length: 7,


      flight: true,
      featured: true,
      description:
 "Italy, country of south-central Europe, occupying a peninsula that juts deep into the Mediterranean Sea. Italy comprises some of the most varied and scenic landscapes on Earth and is often described as a country shaped like a boot. At its broad top stand the Alps, which are among the world’s most rugged mountains. Italy’s highest points are along Monte Rosa, which peaks in Switzerland, and along Mont Blanc, which peaks in France. The western Alps overlook a landscape of Alpine lakes and glacier-carved valleys that stretch down to the Po River and the Piedmont. Tuscany, to the south of the cisalpine region, is perhaps the country’s best-known region. From the central Alps, running down the length of the country, radiates the tall Apennine Range, which widens near Rome to cover nearly the entire width of the Italian peninsula. South of Rome the Apennines narrow and are flanked by two wide coastal plains, one facing the Tyrrhenian Sea and the other the Adriatic Sea. Much of the lower Apennine chain is near-wilderness, hosting a wide range of species rarely seen elsewhere in western Europe, such as wild boars, wolves, asps, and bears. The southern Apennines are also tectonically unstable, with several active volcanoes, including Vesuvius, which from time to time belches ash and steam into the air above Naples and its island-strewn bay. At the bottom of the country, in the Mediterranean Sea, lie the islands of Sicily and Sardinia.",
       extras: [
        "Travel Management ",
        "Passenger assistance",
        
        "Marketing and internal communication",
        "Adequate safety/security",
        "Internet",
        
      ],
      images: [
        {
          fields: {
            file: {
              url: pic13
            }
          }
        },
        {
          fields: {
            file: {
              url: pic14
            }
          }
        },
        {
          fields: {
            file: {
              url: pic15
            }
          }
        },
        {
          fields: {
            file: {
              url: pic16
            }
          }
        }

      ]
    }
  },
  {
    sys: {
      id: "5"
    },
    fields: {
      name: "France",
      slug: "France",
      type: "European",
      price: 1500,
      length: 10,


      flight: false,
      featured: false,
      description:
 "France is one of Europe's largest countries. It is bordered by six countries other nations: Germany, Belgium and Luxembourg to the northeast, Switzerland and Italy to the southeast and Spain to the southwest. The United Kingdom borders France via the English Channel.",
       extras: [
         
        "Passenger assistance",
        "Consulting",
         
        "Adequate safety/security",
        "Internet",
        "Leisure Services"
      ],
      images: [
        {
          fields: {
            file: {
              url: pic17
            }
          }
        },
        {
          fields: {
            file: {
              url: pic18
            }
          }
        },
        {
          fields: {
            file: {
              url: pic19
            }
          }
        },
        {
          fields: {
            file: {
              url: pic20
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "6"
    },
    fields: {
      name: "England",
      slug: "England",
      type: "European",
      price: 1600,
      length: 15,


      flight: true,
      featured: false,
      description:
 "England became a unified state in AD 927 and after the Age of Discovery has had a significant cultural and legal impact on the wider world. England was where the English language, the Anglican Church and English law, which forms the basis of the common law legal systems of countries around the world, developed." ,
      extras: [
        "Travel Management ",
         
        "Consulting",
        "Marketing and internal communication",
        
        "Internet",
        "Leisure Services"
      ],
      images: [
        {
          fields: {
            file: {
              url: pic21
            }
          }
        },
        {
          fields: {
            file: {
              url: pic22
            }
          }
        },
        {
          fields: {
            file: {
              url: pic23
            }
          }
        },
        {
          fields: {
            file: {
              url: pic24
            }
          }
        }
      ]
    }
  },
  //Asian
  {
    sys: {
      id: "7"
    },
    fields: {
      name: "Japan",
      slug: "Japan",
      type: "Asian",
      price: 900,
      length: 5,


      flight: false,
      featured: true,
      description:
      "If you have been dreaming of traveling to Japan and want to start planning your vacation, this travel guide is packed with all the tips and information that you'll need to know before your trip to the land of the rising sun. From foods to try in Japan, to how much money to budget, to what travel gear to pack, to the best things to do in Japan, we're covering everything. And I mean everything! We're even answering your embarrassing questions like, What are the toilets like in Japan? We are sharing exactly how much it costs to travel to Japan, and we're throwing in some money-saving budget tips! This is the ultimate resources with everything you need to know before visiting Japan for the first time. ",
       extras: [

        "Travel Management ",
        
        "Consulting",
        "Marketing and internal communication",
        
         
        "Leisure Services"
      ],
      images: [
        {
          fields: {
            file: {
              url: pic25
            }
          }
        },
        {
          fields: {
            file: {
              url: pic26
            }
          }
        },
        {
          fields: {
            file: {
              url: pic27
            }
          }
        },
        {
          fields: {
            file: {
              url: pic28
            }
          }
        }

      ]
    }
  },
  {
    sys: {
      id: "8"
    },
    fields: {
      name: "China",
      slug: "China",
      type: "Asian",
      price: 800,
      length: 10,


      flight: true,
      featured: false,
      description:
 "Whether you are looking for ancient history, urban wonders, picturesque landscapes, or cultural experiences; more and more world travelers are turning their toes towards China. For first time travelers to this beautiful and historic land, here are some facts you may wish to know." ,
      extras: [
        "Travel Management ",
        "Passenger assistance",
        
        "Marketing and internal communication",
        "Adequate safety/security",
        "Internet",
        
      ],
      images: [
        {
          fields: {
            file: {
              url: pic29
            }
          }
        },
        {
          fields: {
            file: {
              url: pic30
            }
          }
        },
        {
          fields: {
            file: {
              url: pic31
            }
          }
        },
        {
          fields: {
            file: {
              url: pic32
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "9"
    },
    fields: {
      name: "Singapore",
      slug: "Singapore",
      type: "Asian",
      price: 700,
      length: 5,


      flight: true,
      featured: false,
      description:
 "For many travelers, Singapore is their first introduction to Southeast Asia, and the island nation seems to exist in two worlds at once. When you visit Singapore, the sparkling boutiques and department stores on Orchard Road and the hotels, both historic and new, may remind you more of Houston, Texas, than of the inscrutable East. But Singapore travel has its exotic elements too - in the shophouses of Chinatown, for instance, as well as the temples of Little India and the city's night markets. Whether you're stopping in Singapore en route to other parts of Asia or are making it the only stop on your journey, you'll be greatly rewarded if you delve beneath the city's spotless surface and get to know the real Singapore.",
       extras: [
        "Travel Management ",
         
        
        "Marketing and internal communication",
        "Adequate safety/security",
        "Internet",
        "Leisure Services"
      ],
      images: [
        {
          fields: {
            file: {
              url: pic33
            }
          }
        },
        {
          fields: {
            file: {
              url: pic34
            }
          }
        },
        {
          fields: {
            file: {
              url: pic35
            }
          }
        },
        {
          fields: {
            file: {
              url: pic36
            }
          }
        }
      ]
    }
  },
   //Africa
  {
    sys: {
      id: "10"
    },
    fields: {
      name: "Egypt",
      slug: "Egypt",
      type: "Africa",
      price: 1000,
      length: 10,


      flight: false,
      featured: true,
      description:
 "Egypt is not only a country of antiquity, but also of magnificent landscapes offering cities and oases, deserts and beaches, ancient obelisks and modern hotels. Bordered by the Mediterranean, the Red Sea and the fabled Nile River, you can experience eras as far back as the dawn of civilization.",
       extras: [
        "Travel Management ",
        
        "Consulting",
         
        "Adequate safety/security",
        "Internet",
        "Leisure Services"
      ],
      images: [
        {
          fields: {
            file: {
              url: pic37
            }
          }
        },
        {
          fields: {
            file: {
              url: pic38
            }
          }
        },
        {
          fields: {
            file: {
              url: pic39
            }
          }
        },
        {
          fields: {
            file: {
              url: pic40
            }
          }
        }
      ]
    }
    },
    {
      sys: {
        id: "11"
      },
      fields: {
        name: "South Africa",
        slug: "South Africa",
        type: "Africa",
        price: 1600,
        length: 14,

        flight: true,
        featured: false,
        description:
          "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
        extras: [
        "Travel Management ",
        "Passenger assistance",
        "Consulting",
        "Marketing and internal communication",
        "Adequate safety/security",
        "Internet",
        "Leisure Services"
        ],
        images: [
          {
            fields: {
              file: {
                url: pic41
              }
            }
          },
          {
            fields: {
              file: {
                url: pic42
              }
            }
          },
          {
            fields: {
              file: {
                url: pic43
              }
            }
          },
          {
            fields: {
              file: {
                url: pic44
              }
            }
          }
        ]
      }
    },

    {
      sys: {
        id: "12"
      },
      fields: {
        name: "republic of mauritius",
        slug: "republic of mauritius",
        type: "Africa",
        price: 1300,
        length: 10,
         

        flight: true,
        featured: false,
        description:
 "There are 700 species of indigenous plants including over 60 different orchid species. Interestingly, Mauritius is the second largest supplier of plants and cut flowers outside of the Netherlands. A great deal of Mauritius’ natural flora is endangered because of the destruction of habitats as well as the introduction of plants and animals that compete and destroy fruits and seedlings.",
         extras: [
          "Travel Management ",
          "Passenger assistance",
           
          "Marketing and internal communication",
          "Adequate safety/security",
          "Internet",
          "Leisure Services"
        ],
        images: [
          {
            fields: {
              file: {
                url: pic45
              }
            }
          },
          {
            fields: {
              file: {
                url: pic46
              }
            }
          },
          {
            fields: {
              file: {
                url: pic47
              }
            }
          },
          {
            fields: {
              file: {
                url: pic48
              }
            }
          }
        ]
      }
    }

];
