import React, { Component } from 'react'
import items from "./data";
import Client from './Contentful';

const TravelContext = React.createContext();


// let example = {};
// Client.getEntries({
//   content_type:"reactExample"
// })
// .then((response) => {
//   //console.log(response.items);
//   example = response.items;
//   console.log(example);
// }).catch(console.error);


export default class TravelProvider extends Component {
 state={
    travels:[],
    sortedTravels:[],
    FeaturedTravels:[],
    loading:true,
    type:'all',
    price: 0,
    minPrice:0,
    maxPrice:0,
    minlength:0,
    maxlength: 0,
    flight: false
 }




// getData = async () => {
//     try {
//       let response = await Client.getEntries({
//         content_type: "reactExample",
//         //order : "sys.createdAt",
//         //order :"-fields.price" //reverse order
//         order :"fields.price"
//       });
//       let travels = this.formatData(response.items);
//       let FeaturedTravels = travels.filter(travel => travel.featured === true);
//       //
//       let maxPrice = Math.max(...travels.map(item => item.price));
//       let maxlength = Math.max(...travels.map(item => item.length));
//       this.setState({
//         travels,
//         FeaturedTravels,
//         sortedTravels: travels,
//         loading: false,
//         //
//         price: maxPrice,
//         maxPrice,
//         maxlength
//       });
//     } catch (error) {
//       console.log(error);
//     }
//   };

// componentDidMount(){
//   this.getData();
// }

 componentDidMount(){
   //this.getDatat
   let travels = this.formatData(items)
    //console.log(travels);
    let FeaturedTravels = travels.filter(travel=>travel.featured === true);
    let maxPrice = Math.max(...travels.map(item=>{
      return item.price
    }))
    let maxlength = Math.max(...travels.map(item=>{
      return item.length
    }))
    this.setState(
      {
        travels,
        FeaturedTravels,
        sortedTravels:travels,
        loading: false,
        price: maxPrice,
        maxPrice,
        maxlength
      }
    )


 }

 formatData(items){
   let tempItems = items.map(item=>{
     let id = item.sys.id;
     let images = item.fields.images.map(image=>
       image.fields.file.url
     );
     let travel = {...item.fields, images, id}

     return travel;
   });
   return tempItems;
 }

 getTravel = slug => {
  let temptravels = [...this.state.travels];
  const travel = temptravels.find(travel => travel.slug === slug);
  return travel;
};

handleChange = event=>{
  const target = event.target;
  const value = target.type =='checkbox' ? target.checked: target.value;
  const name = event.target.name;
  console.log(target, value, name);
  this.setState({
       [name]: value  //use [name]  can be stand for every filter
   }, this.filtertravels)
}

filtertravels = ()=>{
  let {travels, type,  price,minlength, maxlength, flight, pets} = this.state
  //all the roms
  let temptravels = [...travels];
  //transform value

  price = parseInt(price);
  //filter by type
  if(type !== 'all')
  {
    temptravels = temptravels.filter(travel=>travel.type=== type);
  }

  //filter by price
  temptravels = temptravels.filter(travel=>travel.price<=price);
  //filter by length
  temptravels = temptravels.filter(
    travel => travel.length >= minlength && travel.length <= maxlength
  );
  //filter by flight
  if (flight) {
    temptravels = temptravels.filter(travel => travel.flight === true);
  }
  //filter by pets
  if (pets) {
    temptravels = temptravels.filter(travel => travel.pets === true);
  }

  //chane state
  this.setState({
    sortedTravels: temptravels
  })
}



 render() {


  return (
   <TravelContext.Provider value={{
     ...this.state,
    getTravel:this.getTravel,
    handleChange: this.handleChange
  }}>
    {this.props.children}

   </TravelContext.Provider>
  )
}
}

const TravelConsumer = TravelContext.Consumer;



export function withTravelConsumer(Component){
  return function ConsumerWrapper(props){
    return <TravelConsumer>
      {value=> <Component {...props} context={value}/>}
    </TravelConsumer>
  }
}

export {TravelProvider,TravelConsumer,TravelContext};
