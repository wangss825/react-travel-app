import React from 'react'
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';

import Hero from '../components/Hero';

import Contact from "../components/Contact";


export default function Register() {
 return (
  <>
  <Hero hero='aboutHero' >
  <Banner title="Register" >
    <Link to='/' className='btn-primary'>
     Return home
    </Link>
  </Banner>
  </Hero>

  <h1>Register page: under constructon</h1>

  <Contact />
  </>
 )
}
