import React from 'react'
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';

import Hero from '../components/Hero';

import Contact from "../components/Contact";


export default function About() {
 return (
  <>
  <Hero hero='aboutHero' >
  <Banner title="About us" >
    <Link to='/' className='btn-primary'>
     Return home
    </Link>
  </Banner>
  </Hero>

  <h1>Contact page: under construction</h1>

  <Contact />
  </>
 )
}
