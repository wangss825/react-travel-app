import React from 'react';
import Hero from '../components/Hero';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
import Services from "../components/Services";
import FeaturedTravel from '../components/FeaturedTravel';
import Contact from "../components/Contact";


export default function Home(){
  return(
  <>
    <Hero>
      <Banner title="Travel around the world" subtitle="let's go!!!">
      <Link to='/travels' className='btn-primary'>
        Our Travels
      </Link>
      </Banner>
    </Hero>


    <FeaturedTravel />
    <Services />
    <Contact />




</>

  )



}
