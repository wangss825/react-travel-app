import React from 'react';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';

import Hero from '../components/Hero';
import TravelsContainer from "../components/TravelsContainer";
import Contact from "../components/Contact";

export default function Travels(){
  return (
  <>
  <Hero hero='travelsHero' >
  <Banner title="our travels" >
    <Link to='/' className='btn-primary'>
    return home
    </Link>
    </Banner>
  </Hero>

  

  <TravelsContainer />
  <Contact />

  </>


  )



}
