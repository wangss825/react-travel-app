import React from 'react'
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';

import Hero from '../components/Hero';

import Contact from "../components/Contact";


export default function Login() {
 return (
  <>
  {/* <Hero hero='loginHero'>
  </Hero> */}
   <div className="login-form">
   <div className="or"><h2>Login </h2></div>
      <form className="login-center">
        <div>
            <label className="login-label">Username:</label>
            <input className="login-input" type="email" />
        </div>    
        <div>
            <label className="login-label">Password:</label>
           < input className="login-input" type="password" />
        </div>
                
        <br />
        <div>
            <input className="login-submit" type="submit" value="Submit" />
        </div>
        <div className="or">or</div>
        <div>
            <input className="register-btn" type="submit" value="Register"/>
        </div>
      </form>
     
      

  </div>

   

  <Contact />
  </>
 )
}
