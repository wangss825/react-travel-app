import React, { Component } from "react";
import defaultBcg from "../images/defaultBcgHome.jpg";
import Hero from "../components/Hero";
import Banner from "../components/Banner";
import {Link} from 'react-router-dom';
import { TravelContext} from "../context";
import StyledHero  from "../components/StyledHero";
import Contact from "../components/Contact";

export default class Singletravel extends Component{
  constructor(props){
    super(props);
    console.log(this.props);
    this.state = {
      slug: this.props.match.params.slug,
      defaultBcg
    }


  }

  static contextType = TravelContext;

  render(){
    const {getTravel} = this.context;
    console.log(this.state.slug);
    const travel = getTravel(this.state.slug);
    //console.log(travel);
    if(!travel)
    {
      return <div className="error">
        <h3>no such travel could be found</h3>
        <Link to={`/`} className="btn-primary" >Go back to travel page</Link>

      </div>;

    }
    const {name, description, length, price, extras, flight, pets, images}= travel;
    const [mainImg, ...defaultImg] = images;

    return  (
      <>
      <StyledHero  img={images[0] || this.state.defaultBcg}>
        <Banner title={`${name} `}>
          <Link to="/travels" className="btn-primary">
            back to travels
          </Link>
        </Banner>

      </StyledHero>
      <section className="single-travel">
        <div className="single-travel-images">
        {
            defaultImg.map((item, index)=>{
              return <img key={index} src={item} alt={name}/>
            }
        )}
        </div>
        <div className="single-travel-info">
        <article className="info">
          <h3>Info</h3>
          <h6>price: ${price}</h6>
          <h6>length: {length} days</h6>
          <h6>{flight && "free flight included"}</h6>
        </article>
          <article className="desc">
            <h3>details</h3>
            <p>{description}</p>
          </article>
        </div>
      </section>
      <section class="travel-extras">
        <h6>extras</h6>
            <ul className="extras">
            {extras.map((item, index)=>(
                 <li key={index}>-{item}</li>
            ))}

            </ul>
      </section>
      <Contact />
      </>
    )

  }
}
