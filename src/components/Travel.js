import React from 'react';
import { Link } from "react-router-dom";
import defaultImg from "../images/defaultBcgHome.jpg"
import PropTypes from 'prop-types';
import Contact from "./Contact";



export default function Travel({ travel }) {

  //console.log(travel);
  const { name, slug, images, price } = travel;


 return (
   <>
 <article className='travel'>
  <div className="img-container">


   <Link to={`/travels/${slug}`} className='travel-link' >
   <img src={images[0]|| defaultImg} alt="single travel" />
   </Link>


  </div>
  <p className="travel-info">{name}</p>

  <h6>${price}</h6>

  </article>


  </>

 )

}

Travel.propTypes = {
  travel:PropTypes.shape({
    name:PropTypes.string.isRequired,
    slug:PropTypes.string.isRequired,
    images:PropTypes.arrayOf(PropTypes.string).isRequired,
    price:PropTypes.number.isRequired
  })
};
