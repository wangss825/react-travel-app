
// import React from 'react'
// import travelsFilter from './travelsFilter.js';
// import travelsList from'./travelsList';
// import { withTravelConsumer } from '../context';
// import Loading from "./Loading";


// function travelContainer({context}){
//  const {loading, sortedTravels, travels} = context;
//  console.log(travels);
//  if(loading)
//  {
//   return <Loading />
//  }
//  return (
//    <>
//     <travelsFilter travel={travels} />
//     <travelsList travel={sortedTravels}/>
//    </>
//  )
// }
// export default withTravelConsumer(travelContainer);


import React from 'react'
import TravelsFilter from './TravelsFilter.js';
import TravelsList from'./TravelsList';
import { TravelConsumer } from '../context';
import Loading from "./Loading";

export default function travelContainer() {

 return (
  <TravelConsumer>
   {value=>{

    //console.log(value);
    const {loading, sortedTravels, travels} = value;

    if(loading)
    {
     return <Loading />
    }

    //console.log(travels);
    return (
      <>
      <div className='rowC'>
      <TravelsFilter travels ={travels} />
       <TravelsList travels={sortedTravels}/>

       </div>
      </>
    )
   }}

  </TravelConsumer>




 )
}
