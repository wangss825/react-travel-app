 import React, { Component } from 'react'

 import { FaCocktail, FaHiking, FaShuttleVan, FaBeer }
 from 'react-icons/fa';
 import Title from './Title';


 export default class Services extends Component {
  state={
   services:[
    {
     icon:<FaCocktail />,
      title:"About",
      info:"We are the first premium travel service that specializes in SMALL GROUP tour, PRIVATE tour, CUSTOMISED tour to Mekong Delta for 1, 2, 3 days..."

    },
    {
     icon:<FaHiking />,
      title:"history",
      info:"Having been in the tourism industry since 1994, KIM TRAN Travel - with the motto of working with friendly, honesty, hospitality which describe our devotion for creating a unique and unforgettable experience for our customers "

    },
    {
     icon:<FaShuttleVan />,
      title:"What we offer?",
      info:"High quality vacations throughout Indochina, including Vietnam, Cambodia and LaosCultural and physical adventure holidays in Vietnam Cambodia and LaosBeach"
    },
    {
     icon:<FaBeer />,
      title:"What do we facilitate?",
      info:"Hotel and resort booking, Airfare and ground transportation reservation. Tourguide service Visa arrangement Translator/ interpreter service Event organizer/ Charity trip facilitator "

    },
    {
     icon:<FaBeer />,
      title:"What we value?",
      info:"We always believe that we are not merely showing new places to our fellow customers, but actually extending our knowledge of different ethnic cultures in Indo china to our customers."
    }

   ]
  }

  render() {
   return (
    <section className='services'>
     <Title title="services"/>
     <div className='services-center'>
      {this.state.services.map((item,index)=>{
          return <article key={index} className='service'>
          <span>{item.icon}</span>
          <h6>{item.title}</h6>
          <p>{item.info}</p>
          </article>
      })}
     </div>
    </section>
   )
  }
 }
