import React from 'react'
import Travel from './Travel';


export default function TravelsList({travels}) {

 //console.log(travels);

 if(travels.length === 0)
 {
  return (
   <div className="empty-search">
    <h3>unfortunately no travels matched your search</h3>
   </div>
  )
 }

 return (
  <section className='travellist'>
   <div className="travelslist-center">
    {
       travels.map(item=>{
        return <Travel key={item.id} travel={item} />
       })
    }

   </div>
  </section>

 )
}
