 import React from 'react'
 import {useContext} from 'react';
 import {TravelContext} from '../context';
 import Title from "../components/Title";


//get all unique values
const getUnique = (items, value)=>{
    return [... new Set(items.map(item=>item[value]))] //get only the type. not item

}

export default function TravelsFilter({travels}) {

const context = useContext(TravelContext);
  //console.log(context);
const {handleChange, type,  price, minPrice, maxPrice, minlength, maxlength, flight, pets} = context;

//for selection type
 //get unique types
let types = getUnique(travels, 'type');
  //console.log(types);
  //add all
   types = ['all',...types];
 //map to jsx
 types = types.map((item, index)=>{
  return (
  <option value = {item} key={index}>{item}</option>
  );
 });



  return (
   <section className="filter-container">
     <Title title="search travels"  />
    <form className='filter-form'>

     {/* select type */}
     <div className='form-group'>
      <label htmlFor="type">Desitination:</label>
      <select
       name="type"
       id="type"
       value={type}
       onChange={handleChange}
       className="form-control">
       {types}
      </select>
     </div>
     {/* end select type */}


     {/* travel price  */}
     <div className="form-group">
      <label htmlFor ="price">
       Travel price: ${price}
      </label>
      <input type='range'  name='price' min={minPrice} max={maxPrice}
      id="price" value={price} onChange={handleChange} className="form-control" />
     </div>
     {/* end travel price */}


     {/* travel  days */}
     <div className="form-group">
      <label htmlFor="length" >
       Length(days):
      </label>
      <div className="length-inputs">
      <label htmlFor="minlength">Min days :
      <input type='number'  name='minlength' id="minlength"
       value={minlength} onChange={handleChange} className="length-input" />
       </label>
       <label htmlFor="maxlength">Max days :
      <input  type='number'  name='maxlength' id="maxlength"
       value={maxlength}  onChange={handleChange} className="length-input" />
      </label>
      </div>
     </div>
     {/* end travel length */}


      
     {/* extra */}
     <div className="form-group">
      <div className="single-extra">
      <label htmlFor="flight">Including Flight</label>
            <input type="checkbox" name="flight"
            id="flight" checked={flight} onChange={handleChange} />                    
       </div>
    </div>

       {/* end extra */}


    </form>
   </section>
  )
 }
