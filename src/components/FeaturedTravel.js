import React, { Component } from 'react'

import { TravelContext } from '../context';
import Loading from "./Loading";
import Travel from './Travel';
import Title from './Title';

export default class FeaturedTravel extends Component {

static contextType = TravelContext;

 render() {

  let {loading, FeaturedTravels:travels} = this.context;
  //console.log(travels);
  travels = travels.map(travel=>{
   return <Travel key={travel.id} travel={travel} />;
  });

  return (
   <section className="featured-travels">

     <Title title="Our featured Destination" />
     <div className="featured-travels-center" >
      {loading?<Loading />: travels}

     </div>



   </section>
   
  )
 }
}
