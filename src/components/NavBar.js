import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FaAlignRight } from "react-icons/fa";
import { slide as Menu } from 'react-burger-menu'
//import logo from "../images/logo.png";
import { BsSearch } from 'react-icons/bs';
import { GiBeachBall } from 'react-icons/gi';
import {FaBars} from 'react-icons/fa';
export default class Navbar extends Component {
  state = {
    isOpen: false
  };
  handleToggle = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };
  render() {
    return (
      <>
      <nav className="navbar">

        <div className="nav-center">
          <div className="nav-header">
            <button
              type="button"
              className="nav-btn"
              onClick={this.handleToggle}>
              <FaAlignRight className="nav-icon" />
            </button>
          </div>




          <ul className={this.state.isOpen ? "nav-links show-nav" : "nav-links"}>

            <li className="logo-nav">                 
                  <GiBeachBall className="logo-icon" />                                       
            </li>
            <li onClick={this.handleToggle}>
              <Link to="/">Home</Link>
            </li>
            <li onClick={this.handleToggle}>
              <Link to="/travels">Travels</Link>
            </li>
            <li onClick={this.handleToggle}>
              <Link to="/about">About</Link>
            </li>
          </ul>
        </div>

        
        {/* <div>
          <ul>
            <li className="logo">                 
                  <GiBeachBall className="logo-icon" />                                       
              </li>
          </ul>
        </div> */}


        <div className="nav-log">
          <ul className="nav-log-link">          
             <li className='nav-search-reg'>
             <Link to="/travels">
                <button type="button" className="nav-search-btn"> 
                  <BsSearch className="nav-icon" />             
                </button>  
              </Link>                  
             </li>
            <li className='nav-search-reg'>
              <Link to="/login">Login</Link>
            </li>
          </ul>
        </div>

      </nav>
      </>

    );
  }
}
