import {createClient}  from 'contentful'
export default createClient({
 space: process.env.REACT_APP_API_SPACE,
 accessToken: process.env.REACT_APP_ACCESS_TOKEN
 
});

// const contentful = require("contentful");
//  const Client = contentful.createClient({
//   // This is the space ID. A space is like a project folder in Contentful terms
//   space: "popp49g3md7g",
//   // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
//   accessToken: "1F2JEmZQKiNgggyKpk2iLXgTlMQXJexgfFRuhxot0GQ"
// });